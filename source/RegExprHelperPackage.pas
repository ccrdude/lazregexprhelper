{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit RegExprHelperPackage;

interface

uses
   RegRegExprHelper,
   {%H-}RegExprIDE.Form.Main,
   LazarusPackageIntf;

implementation

procedure Register;
begin
   RegisterUnit('RegRegExprHelper', @RegRegExprHelper.Register);
end;

initialization
   RegisterPackage('RegExprHelperPackage', @Register);
end.
